package com.example.azzarnoy.classwork;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    public ArrayList<Names> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i = 0; i < 10;i++){

            names.add(new Names("Valera"+ i, "@mail"));
        }
        RecyclerView list = (RecyclerView) findViewById(R.id.listView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.setAdapter(new RecyclerViewAdapter());



    }

    class Names {
        String name;
        String eMail;

        public Names(String name, String email) {
            this.name = name;
            this.eMail = email;
        }

        public String getName(){
            return name;
        }
        public void setName(String name){

            this.name = name;
        }
        public String getEmail(){
            return eMail;
        }
        public void setEmail(String eMail){

            this.eMail = eMail;
        }
    }



    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
            View v = getLayoutInflater().inflate(R.layout.list_item, parent,false);
            return new ViewHolder(v);
            String;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            holder.name.setText(names.get(i).getName());
            holder.eMail.setText(names.get(i).getEmail());

        }

        @Override
        public int getItemCount() {
            return names.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            private TextView name;
            private TextView eMail;

            public ViewHolder(View itemView) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.textViewName);
                eMail = (TextView) itemView.findViewById(R.id.textViewEmail);
            }
        }
    }


}
